import 'dart:io';
import 'dart:typed_data';

import 'package:budget_onderzoek/database/datamodels/transaction_db.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  DatabaseHelper._();
  static final DatabaseHelper instance = DatabaseHelper._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    String path = await _getDatabasePath();
    await _importDatabase();
    return await openDatabase(path);
  }

  Future<String> _getDatabasePath() async {
    Directory databasesDir = await getApplicationDocumentsDirectory();
    String path = join(databasesDir.path, 'budget_nl.db');
    return path;
  }

  Future<void> _importDatabase() async {
    String path = await _getDatabasePath();
    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data = await rootBundle.load(join('assets', 'budget_nl.db'));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
      _database = await openDatabase(path);
      TransactionDatabase.createTables(_database);
    }
  }
}
