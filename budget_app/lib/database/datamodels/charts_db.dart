import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;

class ChartData {
  static List<Map<String, dynamic>> tblCoicop;

  static Future<void> loadChartTables() async {
    Database db = await DatabaseHelper.instance.database;

    if (tblCoicop == null) {
      tblCoicop = await db.query(
        "tblCoicop",
      );
    }
  }

  static Map<int, Color> categoryColors = {
    0: ColorPallet.pink,
    1: ColorPallet.darkGreen,
    2: ColorPallet.orange,
    3: ColorPallet.lightGreen,
    4: ColorPallet.darkBlue,
    5: ColorPallet.lightBlue,
    6: ColorPallet.midblue,
    7: ColorPallet.pink.withOpacity(0.5),
    8: ColorPallet.darkGreen.withOpacity(0.5),
    9: ColorPallet.orange.withOpacity(0.5),
    10: ColorPallet.lightGreen.withOpacity(0.5),
    11: ColorPallet.darkBlue.withOpacity(0.5),
    12: ColorPallet.lightBlue.withOpacity(0.5),
    13: ColorPallet.midblue.withOpacity(0.5),
  };

  static Future<String> getTotalSpend() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> tblProducts = await db.query(
      "products",
    );
    await loadChartTables();
    getCategorySpending();
    double totalSpend = 0;
    tblProducts.forEach((product) {
      totalSpend = totalSpend + double.parse(product['price']);
    });
    return totalSpend.toString();
  }

  static Future<List<Map<String, dynamic>>> getCategorySpending() async {
    String _totalSpend = await getTotalSpend();
    double totalSpend = double.parse(_totalSpend);

    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> tblProducts = await db.query(
      "products",
    );

    List<Map<String, dynamic>> category1 = [];
    tblCoicop.forEach((coicop) {
      if (coicop['nivo2'] == "") {
        category1.add({
          "code": coicop['code'],
          "name": coicop['coicop'],
          "spendTotal": 0,
          "percentage": "0",
          "color": ""
        });
      }
    });

    tblProducts.forEach((product) {
      category1.forEach((category) {
        if (category['code'] == product['productCode'].split(".")[0]) {
          category['spendTotal'] =
              category['spendTotal'] + double.parse(product['price']);

          category['percentage'] = double.parse(
                  ((category['spendTotal'] / totalSpend) * 100).toString())
              .toStringAsFixed(0);
        }
      });
    });

    category1.sort((a, b) {
      return b['spendTotal'].compareTo(a['spendTotal']);
    });

    int colorCounter = 0;
    category1.forEach((category) {
      category['color'] = categoryColors[colorCounter];
      colorCounter++;
      if (colorCounter == 14) colorCounter = 0;
    });

    return category1;
  }

  static Future<List<charts.Series<ExpenseCategory, String>>>
      getDonutChartData() async {
    List<Map<String, dynamic>> category1 = await getCategorySpending();
    List<ExpenseCategory> data = [];
    category1.forEach((category) {
      int value = int.parse(
          double.parse(category['spendTotal'].toString()).toStringAsFixed(0));
      data.add(ExpenseCategory(category['name'].toString(), value,
          getChartColor(category['color'])));
    });

    final finalData = data;

    return [
      charts.Series<ExpenseCategory, String>(
        id: 'Category',
        domainFn: (ExpenseCategory category, _) => category.categoryName,
        measureFn: (ExpenseCategory category, _) => category.moneySpent,
        data: finalData,
        colorFn: (ExpenseCategory category, _) => category.color,
        labelAccessorFn: (ExpenseCategory category, _) =>
            '€${category.moneySpent}',
        outsideLabelStyleAccessorFn: (ExpenseCategory category, _) =>
            charts.TextStyleSpec(
                fontFamily: 'Source Sans Pro Bold',
                fontSize: (16).floor(), // size in Pts.
                color: getChartColor(ColorPallet.darkBlue)),
      )
    ];
  }

  static charts.Color getChartColor(Color inputColor) {
    return charts.Color(
        r: inputColor.red,
        g: inputColor.green,
        b: inputColor.blue,
        a: inputColor.alpha);
  }
}

class ExpenseCategory {
  final String categoryName;
  final int moneySpent;
  final charts.Color color;

  ExpenseCategory(this.categoryName, this.moneySpent, this.color);
}
