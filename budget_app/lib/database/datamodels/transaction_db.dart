import 'dart:async';

import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class TransactionDatabase {
  String transactionID = "";
  static String tableTransaction = 'transactions';
  String store = "";
  String storeType = "";
  String date = "";
  String discountAmount = "";
  String discountPercentage = "";
  String expenseAbroad = "";
  String expenseOnline = "";
  String totalPrice = "";
  static String tableProduct = 'products';
  String product = "";
  String productCategory = "";
  String price = "";
  String discountText = "";
  String receiptLocation = "";
  String productCode = "";

  static void createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableTransaction (
            transactionID TEXT,
            store TEXT,
            storeType TEXT,
            date TEXT,
            discountAmount TEXT,
            discountPercentage TEXT,
            expenseAbroad TEXT,
            expenseOnline TEXT,
            totalPrice TEXT,
            discountText TEXT,
            receiptLocation TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableProduct (
            transactionID TEXT,
            product TEXT,
            productCategory TEXT,
            price TEXT,
            productCode TEXT)
          ''');
  }

  static void insertTransaction(TransactionDatabase trx) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = trx.transactionID;
    data['store'] = trx.store;
    data['storeType'] = trx.storeType;
    data['date'] = trx.date;
    data['discountAmount'] = trx.discountAmount;
    data['discountPercentage'] = trx.discountPercentage;
    data['discountText'] = trx.discountText;
    data['expenseAbroad'] = trx.expenseAbroad;
    data['expenseOnline'] = trx.expenseOnline;
    data['totalPrice'] = trx.totalPrice;
    data['receiptLocation'] = trx.receiptLocation;

    await db.insert(tableTransaction, data);
  }

  static void insertProduct(TransactionDatabase trx) async {
    Database db = await DatabaseHelper.instance.database;
    Map<String, dynamic> data = Map<String, dynamic>();

    data['transactionID'] = trx.transactionID;
    data['product'] = trx.product;
    data['productCategory'] = trx.productCategory;
    data['price'] = trx.price;
    data['productCode'] = trx.productCode;

    await db.insert(tableProduct, data);
  }

  static Future<List<Map<String, dynamic>>> queryAllTransactionRows(
      BuildContext context, FilterModel filtermodel) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results =
        await db.query(tableTransaction, orderBy: "date");

    //If searchword is provided, filter results on search word
    String searchWord = ExpenseListModel.of(context).searchWord;
    if (ExpenseListModel.of(context).searchWord != null) {
      searchWord = searchWord.toLowerCase();
      List<Map<String, dynamic>> resultsWithSearchWord = [];
      results.forEach((transaction) {
        String store = transaction['store'].toString().toLowerCase();
        if (store.contains(searchWord)) {
          resultsWithSearchWord.add(transaction);
        }
      });
      results = resultsWithSearchWord;
    }

    //If value filter is provided, filter results on values
    if (filtermodel.minimumPrice != "" || filtermodel.maximumPrice != "") {
      Map<String, dynamic> dbValues = await getLowestAndHighestValue();
      String minPriceFilter = filtermodel.minimumPrice != ""
          ? filtermodel.minimumPrice
          : dbValues['lowest'];
      String maxPriceFilter = filtermodel.maximumPrice != ""
          ? filtermodel.maximumPrice
          : dbValues['highest'];

      List<Map<String, dynamic>> resultsWithPriceFiltered = [];
      results.forEach((transaction) {
        if (double.parse(transaction['totalPrice']) >=
                double.parse(minPriceFilter) &&
            double.parse(transaction['totalPrice']) <=
                double.parse(maxPriceFilter)) {
          resultsWithPriceFiltered.add(transaction);
        }
      });
      results = resultsWithPriceFiltered;
    }

    //If date filter is provided, filter results on dates
    if (filtermodel.startDate != "" || filtermodel.endDate != "") {
      Map<String, dynamic> dbValues = await getFirstAndLastDate();

      String startDateFilter = filtermodel.startDate != ""
          ? filtermodel.startDate
          : dbValues['lowest'];
      String endDateFilter =
          filtermodel.endDate != "" ? filtermodel.endDate : dbValues['highest'];

      List<Map<String, dynamic>> resultsWithDateFiltered = [];

      results.forEach((transaction) {
        bool firstCheck = !(DateTime.parse(transaction['date'])
            .isBefore(DateTime.parse(startDateFilter)));

        bool secondCheck = !(DateTime.parse(transaction['date'])
            .isAfter(DateTime.parse(endDateFilter)));

        if (firstCheck && secondCheck) {
          resultsWithDateFiltered.add(transaction);
        }
      });
      results = resultsWithDateFiltered;
    }
    return results;
  }

  static Future<List<Map<String, dynamic>>> queryTransactionProducts(
      String transactionID) async {
    Database db = await DatabaseHelper.instance.database;
    return await db.query(
      tableProduct,
      where: "transactionID = ?",
      whereArgs: [transactionID],
    );
  }

  static Future getTransactionInfo(
      String transactionID, NewTransactionModel newTransaction) async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> resultStore = await db.query(tableTransaction,
        where: "transactionID = ?", whereArgs: [transactionID]);
    newTransaction.store = resultStore[0]['store'];
    newTransaction.storeType = resultStore[0]['storeType'];
    newTransaction.date = resultStore[0]['date'];
    newTransaction.discountText = resultStore[0]['discountText'];

    newTransaction.receiptLocation = resultStore[0]['receiptLocation'];

    newTransaction.receiptPrice = resultStore[0]['totalPrice'];

    newTransaction.discountAmount =
        double.parse(resultStore[0]['discountAmount'].toString());

    List<Map<String, dynamic>> resultProducts = await db.query(tableProduct,
        where: "transactionID = ?", whereArgs: [transactionID]);

    resultProducts.forEach((productInfo) {
      newTransaction.product = productInfo['product'];
      newTransaction.productCategory = productInfo['productCategory'];
      newTransaction.price = productInfo['price'];

      newTransaction.addNewProduct();
    });

    newTransaction.discountPercentage =
        double.parse(resultStore[0]['discountPercentage']);
    newTransaction.abroadExpense =
        resultStore[0]['expenseAbroad'].toLowerCase() == 'true';
    newTransaction.onlineExpense =
        resultStore[0]['expenseOnline'].toLowerCase() == 'true';

    newTransaction.totalPrice = double.parse(resultStore[0]['totalPrice']);
  }

  static void deleteTransaction(String transactionID) async {
    Database db = await DatabaseHelper.instance.database;

    db.delete(tableProduct,
        where: "transactionID = ?", whereArgs: [transactionID]);

    db.delete(tableTransaction,
        where: "transactionID = ?", whereArgs: [transactionID]);
  }

  static Future<Map<String, dynamic>> getFirstAndLastDate() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results =
        await db.query(tableTransaction, orderBy: "date", columns: ["date"]);

    String first;
    String last;
    if (results.length >= 1) {
      first = results[0]['date'];
      last = results[results.length - 1]['date'];
    } else {
      first = DateTime.now().toString();
      last = (DateTime.now()).add(Duration(days: 30)).toString();

      first = DateFormat('yyyy-MM-dd').format(DateTime.parse(first));
      last = DateFormat('yyyy-MM-dd').format(DateTime.parse(last));
    }

    return {"first": first, "last": last};
  }

  static Future<Map<String, dynamic>> getLowestAndHighestValue() async {
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> results = await db.query(tableTransaction,
        orderBy: "totalPrice", columns: ["totalPrice"]);

    String lowest;
    String highest;
    if (results.length >= 1) {
      lowest = results[0]['totalPrice'];
      lowest = double.parse(lowest).toStringAsFixed(0);
      highest = results[results.length - 1]['totalPrice'];
      highest = double.parse(highest).toStringAsFixed(0);
    } else {
      lowest = "0";
      highest = "0";
    }

    return {"lowest": lowest, "highest": highest};
  }

  static Future<String> getProductCode(String coicop) async {
    print(456546);
    Database db = await DatabaseHelper.instance.database;
    List<Map<String, dynamic>> code = await db.query("tblProductsNew",
        where: "Coicop = ?", whereArgs: [coicop], columns: ["code"]);

    if (code != null) {
      return code[0]['code'];
    } else {
      return "";
    }
  }
}
