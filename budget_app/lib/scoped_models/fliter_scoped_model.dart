import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class FilterModel extends Model {
  String minimumPrice = "";
  String maximumPrice = "";

  String startDate = "";
  String endDate = "";

  void reset() {
    minimumPrice = "";
    maximumPrice = "";
    startDate = "";
    endDate = "";
    print(124);

    notifyListeners();
  }

  static FilterModel of(BuildContext context) =>
      ScopedModel.of<FilterModel>(context);
}
