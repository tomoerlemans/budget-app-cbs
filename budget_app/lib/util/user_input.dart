//TODO: tegen Barry zeggen dat ik bedragen boven de 100.000.000 niet accepteer en dat ik bedragen boven de 100.000 afrond naar boven de comma

class UserInput {
  //Remove all characters except for numbers
  static String valueSanitizer(String str) {
    str = str.replaceAll(RegExp(','), '.');
    str = str.replaceAll(RegExp('[^0-9.,]+'), '');

    try {
      if (str == "") {
        return "";
      }

      var canParse = double.parse(str).toString();
      return str;

      // if (double.parse(str) > 1000) {
      //   return double.parse(str).toStringAsFixed(0);
      // } else {
      //   return double.parse(str).toStringAsFixed(2);
      // }
    } catch (e) {
      return null;
    }
  }

  //Remove everything except letters and spaces
  //Makes sure that the string is formatted to have the first letter upper case and the rest lower case.
  static String textSanitizer(String str) {
    str = str.replaceAll(RegExp('[^a-zA-Z 0-9]+'), '');
    if (str.length > 1) {
      return '${str[0].toUpperCase()}${str.substring(1).toLowerCase()}';
    } else {
      return str;
    }
  }

  //Needs to match a CoiCop classification.
  static bool textValidator(String str) {
    if (str is String) {
      return true;
    }
    return false;
  }

  static bool intValidator(String str) {
    if (str == null) {
      return false;
    }

    if (str.replaceAll(RegExp('[^0-9]+'), '').length == str.length) {
      if (int.parse(str) >= 0) {
        return true;
      }
    }
    return false;
  }

  //Make sure that the value is a positive double
  static bool valueValidator(String str) {
    if (str == null) {
      return false;
    }

    if (str.length == 1) {
      if (str[0] == ".") {
        return true;
      }
    }

    if (str.length >= 2) {
      if (str[0] == "0" && str[1] != ".") {
        return false;
      }
    }

    if (str.length == 0) {
      return true;
    }

    if (str.contains(" ")) {
      return false;
    }

    try {
      double value = double.parse(str);
      if (value >= 0 && value <= 10000000) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static bool percentageValidator(String str) {
    if (str == null) {
      return false;
    }

    double value = double.parse(str);
    try {
      if (value >= 0 && value <= 100) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
