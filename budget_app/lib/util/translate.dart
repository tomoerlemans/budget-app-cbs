library translate;

final Translate translate = Translate._private();

class Translate {
  String languagePreference;

  Translate._private();

  /*
  OverviewPage
  */

  String get monday => _monday[languagePreference];
  Map<String, String> _monday = <String, String>{"nl": "M", "en": "M"};

  String get tuesday => _tuesday[languagePreference];
  Map<String, String> _tuesday = <String, String>{"nl": "D", "en": "T"};

  String get wednesday => _wednesday[languagePreference];
  Map<String, String> _wednesday = <String, String>{"nl": "W", "en": "W"};

  String get thursday => _thursday[languagePreference];
  Map<String, String> _thursday = <String, String>{"nl": "D", "en": "T"};

  String get friday => _friday[languagePreference];
  Map<String, String> _friday = <String, String>{"nl": "V", "en": "F"};

  String get saturday => _saturday[languagePreference];
  Map<String, String> _saturday = <String, String>{"nl": "Z", "en": "S"};

  String get sunday => _sunday[languagePreference];
  Map<String, String> _sunday = <String, String>{"nl": "Z", "en": "S"};

  String get fixedExpensesFilled => _fixedExpensesFilled[languagePreference];
  Map<String, String> _fixedExpensesFilled = <String, String>{
    "nl": "Vaste uitgaven \n ingevuld:",
    "en": "Fixed expenses \n filled out:"
  };

  String get variablexExpensesFilled =>
      _variablexExpensesFilled[languagePreference];
  Map<String, String> _variablexExpensesFilled = <String, String>{
    "nl": "   Variabele uitgaven:",
    "en": "   Variabele expenses:"
  };

  String get daysCompleted => _daysCompleted[languagePreference];
  Map<String, String> _daysCompleted = <String, String>{
    "nl": "dagen ingevuld",
    "en": "days completed"
  };

  String get daysMissing => _daysMissing[languagePreference];
  Map<String, String> _daysMissing = <String, String>{
    "nl": "dagen ontbreken",
    "en": "days missing"
  };

  String get daysRemaining => _daysRemaining[languagePreference];
  Map<String, String> _daysRemaining = <String, String>{
    "nl": "dagen resterend",
    "en": "days remaining"
  };

  String get info => _info[languagePreference];
  Map<String, String> _info = <String, String>{"nl": "Info", "en": "Info"};

  String get infoTitle => _infoTitle[languagePreference];
  Map<String, String> _infoTitle = <String, String>{
    "nl": "     Informatie budget onderzoek",
    "en": "     Information budget research"
  };

  String get infoBody => _infoBody[languagePreference];
  Map<String, String> _infoBody = <String, String>{
    "nl":
        """Door het gebruik van deze app helpt u mee aan het budget onderzoek van het centraal bureau statistiek.

Als dank voor uw help mogen wij u aan het einde van het onderzoek een VVV bon ter waarde van 10 euro aanbieden. Om in aanraking te komen voor deze bon gelden de volgende regels:

1) Gedurende 30 dagen houdt u iedere dag al uw uitgaven bij in deze app. Zowel uw vaste lasten, als ook variabele uitgaven.
2) Aan het einde van iedere dag verifieert u dat u alle uitgaven heeft bijgewerkt. Ook als u die dag niets heeft uitgegeven.
3) U doet mee aan dit onderzoek binnen de onderzoeks periode: van .... tot .....""",
    "en":
        """Through the use of this app you are helping us conduct research towards household budgets.

As a sign of our appreciation we can offer you a gift certificate worth 10 dollars, if you complete this survey. To be eligable for this gift certifcate you will have to admit to the following rules:

1. For a period of 30 days you keep track of all your expenses. Both your fixed/recurring expenses, such as rent and utilities, as well as variable expenses.
2. At the end of each day you verify that you have kept track of all the expenses for that day. Even if you did not purchaser anything on that particular day.
3. You participate within the research period. Which is between … and …."""
  };

  String get infoForMoreInfo => _infoForMoreInfo[languagePreference];
  Map<String, String> _infoForMoreInfo = <String, String>{
    "nl": "\n\nVoor meer informatie kunt u terecht ",
    "en": "\n\nFor more information please visit  "
  };

  String get infoWebsiteLink => _infoWebsiteLink[languagePreference];
  Map<String, String> _infoWebsiteLink = <String, String>{
    "nl": "op de website van het CBS.",
    "en": "our website."
  };

  /*Navigation*/

  String get overviewPage => _overviewPage[languagePreference];
  Map<String, String> _overviewPage = <String, String>{
    "nl": "Overzicht",
    "en": "Overview"
  };

  String get expensesPage => _expensesPage[languagePreference];
  Map<String, String> _expensesPage = <String, String>{
    "nl": "Uitgaven",
    "en": "Expenses"
  };

  String get insightsPage => _insightsPage[languagePreference];
  Map<String, String> _insightsPage = <String, String>{
    "nl": "Inzichten",
    "en": "Insights"
  };

  String get settingsPage => _settingsPage[languagePreference];
  Map<String, String> _settingsPage = <String, String>{
    "nl": "Instellingen",
    "en": "Settings"
  };

  /*
  Expenses
  */

  String get variableExpenses => _variableExpenses[languagePreference];
  Map<String, String> _variableExpenses = <String, String>{
    "nl": "Variabele uitgaven",
    "en": "Variable expenses"
  };

  String get fixedExpenses => _fixedExpenses[languagePreference];
  Map<String, String> _fixedExpenses = <String, String>{
    "nl": "Vaste lasten",
    "en": "Fixed expenses"
  };

  /*Fliter drawer */
  String get selectFilter => _selectFilter[languagePreference];
  Map<String, String> _selectFilter = <String, String>{
    "nl": "Filters selecteren",
    "en": "Select filters"
  };

  String get date => _date[languagePreference];
  Map<String, String> _date = <String, String>{"nl": "Datum", "en": "Date"};

  String get beginDate => _beginDate[languagePreference];
  Map<String, String> _beginDate = <String, String>{
    "nl": "Start datum",
    "en": "Begin date"
  };

  String get endDate => _endDate[languagePreference];
  Map<String, String> _endDate = <String, String>{
    "nl": "Eind datum",
    "en": "End date"
  };

  String get priceRange => _priceRange[languagePreference];
  Map<String, String> _priceRange = <String, String>{
    "nl": "Bedragen",
    "en": "Price range"
  };

  String get minimum => _minimum[languagePreference];
  Map<String, String> _minimum = <String, String>{
    "nl": "Minimum",
    "en": "Minimum"
  };

  String get maximum => _maximum[languagePreference];
  Map<String, String> _maximum = <String, String>{
    "nl": "Maximum",
    "en": "Maximum"
  };

  String get none => _none[languagePreference];
  Map<String, String> _none = <String, String>{"nl": "Geen", "en": "None"};

  String get categories => _categories[languagePreference];
  Map<String, String> _categories = <String, String>{
    "nl": "Categorieën",
    "en": "Categories"
  };

  String get everything => _everything[languagePreference];
  Map<String, String> _everything = <String, String>{
    "nl": "Alles",
    "en": "Everything"
  };

  String get people => _people[languagePreference];
  Map<String, String> _people = <String, String>{
    "nl": "Personen",
    "en": "People"
  };

  String get fixedOrVariable => _fixedOrVariable[languagePreference];
  Map<String, String> _fixedOrVariable = <String, String>{
    "nl": "Vast en variabel",
    "en": "Fixed or variable"
  };

  String get removeFilters => _removeFilters[languagePreference];
  Map<String, String> _removeFilters = <String, String>{
    "nl": "Filters wissen",
    "en": "Remove filters"
  };

  /*Settings */

  String get settings => _settings[languagePreference];
  Map<String, String> _settings = <String, String>{
    "nl": "Instellingen",
    "en": "Settings"
  };

  String get preferences => _preferences[languagePreference];
  Map<String, String> _preferences = <String, String>{
    "nl": "Voorkeuren",
    "en": "Preferences"
  };

  String get language => _language[languagePreference];
  Map<String, String> _language = <String, String>{
    "nl": "Taal",
    "en": "Language"
  };

  String get currency => _currency[languagePreference];
  Map<String, String> _currency = <String, String>{
    "nl": "Munteenheid",
    "en": "Currency"
  };

  String get family => _family[languagePreference];
  Map<String, String> _family = <String, String>{
    "nl": "Familie",
    "en": "Family"
  };

  String get familyCode => _familyCode[languagePreference];
  Map<String, String> _familyCode = <String, String>{
    "nl": "Familie code",
    "en": "Family code"
  };

  String get shareWithFamily => _shareWithFamily[languagePreference];
  Map<String, String> _shareWithFamily = <String, String>{
    "nl": "Uitgaven delen",
    "en": "Share expenses"
  };

  String get familyMembers => _familyMembers[languagePreference];
  Map<String, String> _familyMembers = <String, String>{
    "nl": "Familie leden",
    "en": "Family code"
  };

  String get add => _add[languagePreference];
  Map<String, String> _add = <String, String>{"nl": "Toevoegen", "en": "Add"};

  String get notifications => _notifications[languagePreference];
  Map<String, String> _notifications = <String, String>{
    "nl": "Notificaties",
    "en": "Notifications"
  };

  String get dailyReminders => _dailyReminders[languagePreference];
  Map<String, String> _dailyReminders = <String, String>{
    "nl": "Dagelijkse herinnering",
    "en": "Daily reminder"
  };

  String get time => _time[languagePreference];
  Map<String, String> _time = <String, String>{"nl": "Tijdstip", "en": "Time"};

  String get contact => _contact[languagePreference];
  Map<String, String> _contact = <String, String>{
    "nl": "Contact",
    "en": "Contact"
  };

  String get phoneNumber => _phoneNumber[languagePreference];
  Map<String, String> _phoneNumber = <String, String>{
    "nl": "Telefoonnummer",
    "en": "Phonenumber"
  };

  String get email => _email[languagePreference];
  Map<String, String> _email = <String, String>{"nl": "E-mail", "en": "E-mail"};

  String get sendMessage => _sendMessage[languagePreference];
  Map<String, String> _sendMessage = <String, String>{
    "nl": "Bericht versturen",
    "en": "Send message"
  };

  String get onValue => _onValue[languagePreference];
  Map<String, String> _onValue = <String, String>{"nl": "Aan", "en": "On"};

  String get languageValue => _languageValue[languagePreference];
  Map<String, String> _languageValue = <String, String>{
    "nl": "Nederlands",
    "en": "English"
  };
}
