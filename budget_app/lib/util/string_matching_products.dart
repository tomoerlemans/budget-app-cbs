import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/util/jaro_winkler.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math';

class StringMatchingProducts {
  static List<Map<String, dynamic>> tblProduct;

  static Future<void> loadProductTable() async {
    Database db = await DatabaseHelper.instance.database;
    if (tblProduct == null) {
      tblProduct = await db.query(
        "tblProductsNew",
      );
    } 
  }

  static Future<List<Map<String, dynamic>>> getSearchResults(
      String searchTerm) async {
    await loadProductTable();

    List<Map<String, dynamic>> rankings = computeEditDistances(searchTerm);
    List<Map<String, dynamic>> results = groupResults(rankings);
    return results;
  }

  static List<Map<String, dynamic>> computeEditDistances(String searchTerm) {
    List<Map<String, dynamic>> result = List<Map<String, dynamic>>();
    tblProduct.forEach((product) {
      if (product['product'][0] == searchTerm[0]) {
        double productScore =
            computeScore(searchTerm, product['product'], product);
        Map<String, dynamic> p = {
          "product": product['product'],
          "coicop": product['coicop'],
          "score_product": productScore,
          "score_coicop": computeScore(searchTerm, product['coicop'], product),
          "frequency": product['frequency'],
        };
        result.add(p);
      }
    });

    result.sort((a, b) {
      int cmp = a['score_product'].compareTo(b['score_product']);
      if (cmp != 0) return cmp;
      return b['frequency'].compareTo(a['frequency']);
    });

    return result;
  }

  static double computeScore(String searchString, String compareString,
      Map<String, dynamic> compareInfo) {
    double jaro = 0;
    searchString = searchString.toLowerCase();
    compareString = compareString.toLowerCase();

    JaroWinkler jaroWinkler = JaroWinkler();
    if (searchString.contains(' ')) {
      jaro = jaroWinkler.normalizedDistance(searchString, compareString);
      if (jaro == 0 && !compareString.contains(' ')) {
        jaro = -1;
      }
    } else {
      List<double> results = [];
      for (final compareWord in compareString.split(' ')) {
        double tempjaro =
            jaroWinkler.normalizedDistance(searchString, compareWord);
        if (tempjaro == 0 && !compareString.contains(' ')) {
          tempjaro = -1;
        }
        results.add(tempjaro);
      }
      jaro = results.reduce(min);
    }
    return jaro;
  }

  static List<Map<String, dynamic>> groupResults(
      List<Map<String, dynamic>> rankings) {
    List<String> coicopResults = [];
    List<Map<String, dynamic>> displayResults = [];

    for (final product in rankings) {
      if (coicopResults.length == 20) {
        break;
      }
      if (!coicopResults.contains(product['coicop'])) {
        coicopResults.add(product['coicop']);
        List<String> productExamples =
            getProductExamples(product['coicop'], rankings);
        displayResults.add({
          'category': product['coicop'],
          'examples': productExamples,
          'score_product': product['score_product'],
          'frequency': product['frequency']
        });
      }
    }
    return displayResults;
  }

  static List<String> getProductExamples(
      String coicop, List<Map<String, dynamic>> rankings) {
    List<String> productExamples = [];
    for (final product in rankings) {
      if (productExamples.length == 1) {
        return productExamples;
      }
      if (product['coicop'] == coicop) {
        productExamples.add(product['product']);
      }
    }
    return productExamples;
  }
}
