import 'dart:async';
import 'package:budget_onderzoek/database/database_helper.dart';
import 'package:budget_onderzoek/util/jaro_winkler.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:math';

class StringMatchingShops {
  static List<Map<String, dynamic>> tblShop;

  static Future<void> loadShopTable() async {
    Database db = await DatabaseHelper.instance.database;
    if (tblShop == null) {
      tblShop = await db.query(
        "tblShop",
      );
    } 
  }

  static Future<List<Map<String, dynamic>>> getSearchResults(
      String searchTerm) async {
    await loadShopTable();
    List<Map<String, dynamic>> rankings = computeEditDistances(searchTerm);
    List<Map<String, dynamic>> results = groupResults(rankings);
    return results;
  }

  static List<Map<String, dynamic>> computeEditDistances(String searchTerm) {
    List<Map<String, dynamic>> result = List<Map<String, dynamic>>();
    tblShop.forEach((shop) {
      double shopScore = computeScore(searchTerm, shop['shop'], shop);
      Map<String, dynamic> p = {
        "shop": shop['shop'],
        "shoptype": shop['shoptype'],
        "score_shop": shopScore,
        "score_shoptype": computeScore(searchTerm, shop['shoptype'], shop),
      };
      result.add(p);
    });

    result.sort((a, b) {
      int cmp = a['score_shop'].compareTo(b['score_shop']);
      return cmp;
    });

    return result;
  }

  static double computeScore(String searchString, String compareString,
      Map<String, dynamic> compareInfo) {
    double jaro = 0;
    searchString = searchString.toLowerCase();
    compareString = compareString.toLowerCase();

    JaroWinkler jaroWinkler = JaroWinkler();
    if (searchString.contains(' ')) {
      jaro = jaroWinkler.normalizedDistance(searchString, compareString);
      if (jaro == 0 && !compareString.contains(' ')) {
        jaro = -1;
      }
    } else {
      List<double> results = [];
      for (final compareWord in compareString.split(' ')) {
        double tempjaro =
            jaroWinkler.normalizedDistance(searchString, compareWord);
        if (tempjaro == 0 && !compareString.contains(' ')) {
          tempjaro = -1;
        }
        results.add(tempjaro);
      }
      jaro = results.reduce(min);
    }
    return jaro;
  }

  static List<Map<String, dynamic>> groupResults(
      List<Map<String, dynamic>> rankings) {
    List<String> shoptypeResults = [];
    List<Map<String, dynamic>> displayResults = [];

    for (final shop in rankings) {
      if (shoptypeResults.length == 20) {
        break;
      }
      if (!shoptypeResults.contains(shop['shoptype'])) {
        shoptypeResults.add(shop['shoptype']);
        List<String> shopExapmples =
            getShopExamples(shop['shoptype'], rankings);
        displayResults.add({
          'category': shop['shoptype'],
          'examples': shopExapmples,
          'score_shop': shop['score_shop'],
        });
      }
    }
    return displayResults;
  }

  static List<String> getShopExamples(
      String shoptype, List<Map<String, dynamic>> rankings) {
    List<String> shopExamples = [];
    for (final shop in rankings) {
      if (shopExamples.length == 3) {
        return shopExamples;
      }
      if (shop['shoptype'] == shoptype) {
        shopExamples.add(shop['shop']);
      }
    }
    return shopExamples;
  }
}
