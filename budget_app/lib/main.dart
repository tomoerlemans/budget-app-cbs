import 'package:budget_onderzoek/pages/main_expenselist.dart';
import 'package:budget_onderzoek/pages/main_insights.dart';
import 'package:budget_onderzoek/pages/main_overview.dart';
import 'package:budget_onderzoek/pages/main_settings.dart';
import 'package:budget_onderzoek/pages/new_camera.dart';
import 'package:budget_onderzoek/pages/new_store.dart';
import 'package:budget_onderzoek/scoped_models/expenselist_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/fliter_scoped_model.dart';
import 'package:budget_onderzoek/scoped_models/transaction_scoped_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';
import 'package:budget_onderzoek/custom_flutter/bottom_navigation_bar.dart'
    as customBottomNavigationBar;

import 'util/translate.dart';

import 'dart:math' as math;

double x;
double y;
double f;

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: ColorPallet.lightBlue),
  );


  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    translate.languagePreference = "nl";
    return ScopedModel<FilterModel>(
      model: FilterModel(),
      child: ScopedModel<ExpenseListModel>(
        model: ExpenseListModel(),
        child: ScopedModel<NewTransactionModel>(
          model: NewTransactionModel(),
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en', 'US'), // English
              const Locale('nl', 'NL'), // Dutch
            ],
            theme: ThemeData(
              primarySwatch: MaterialColor(
                0xFF00A1CD,
                {
                  50: Color(0xFF00A1CD),
                  100: Color(0xFF00A1CD),
                  200: Color(0xFF00A1CD),
                  300: Color(0xFF00A1CD),
                  400: Color(0xFF00A1CD),
                  500: Color(0xFF00A1CD),
                  600: Color(0xFF00A1CD),
                  700: Color(0xFF00A1CD),
                  800: Color(0xFF00A1CD),
                  900: Color(0xFF00A1CD),
                },
              ),
              //fontFamily: 'Source Sans Pro'
            ),
            home: _PageNavigator(),
          ),
        ),
      ),
    );
  }
}

class _PageNavigator extends StatefulWidget {
  @override
  _PageNavigatorState createState() => _PageNavigatorState();
}

class _PageNavigatorState extends State<_PageNavigator>
    with TickerProviderStateMixin {
  int _currentPageIndex = 0;
  AnimationController _controller;

  final List<Widget> _pages = [
    OverviewPage(),
    TransactionsPage(),
    Container(),
    InsightsPage(),
    SettingsPage(),
  ];

  static const List<IconData> icons = const [
    Icons.camera_alt,
    Icons.keyboard
  ];

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
    );
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        height: 210.0 * y,
        width: 63.0 * x,
        margin: EdgeInsets.only(top: 45.0 * y),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(icons.length, (int index) {
            Widget child = Container(
              height: 70.0 *y,
              width: 56.0 *x,
              alignment: FractionalOffset.topCenter,
              child: ScaleTransition(
                scale: CurvedAnimation(
                  parent: _controller,
                  curve: Interval(0.0, 1.0 - index / icons.length / 2.0,
                      curve: Curves.easeOut),
                ),
                child: FloatingActionButton(
                  heroTag: null,
                  backgroundColor: ColorPallet.pink,
                  child: Icon(icons[index], color: Colors.white, size:24*x),
                  onPressed: () {
                    _controller.reverse();
                    _currentPageIndex = 1;
                    NewTransactionModel.of(context).reset();
                    if (icons[index] == Icons.camera_alt) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CameraPage(),
                        ),
                      );
                    } else if (icons[index] == Icons.keyboard) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SelectStorePage(),
                        ),
                      );
                    }
                  },
                ),
              ),
            );
            return child;
          }).toList()
            ..add(
              Container(
                width: 63.0 *x,
                height: 63.0 * y,
                child: FittedBox(
                  child: FloatingActionButton(
                    heroTag: null,
                    child: AnimatedBuilder(
                      animation: _controller,
                      builder: (BuildContext context, Widget child) {
                        return Transform(
                          transform: Matrix4.rotationZ(
                              _controller.value * 0.75 * math.pi),
                          alignment: FractionalOffset.center,
                          child: Icon(
                              _controller.isDismissed ? Icons.add : Icons.add,),
                        );
                      },
                    ),
                    onPressed: () {
                      if (_controller.isDismissed) {
                        _controller.forward();
                      } else {
                        _controller.reverse();
                      }
                    },
                  ),
                ),
              ),
            ),
        ),
      ),
      body: SafeArea(child: _pages[_currentPageIndex]),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Colors.white,
            primaryColor: Colors.blue,
            textTheme: Theme.of(context)
                .textTheme
                .copyWith(caption: new TextStyle(color: Colors.grey))),
        child: customBottomNavigationBar.BottomNavigationBar(
          iconSize: 30.0 * x,
          onTap: (index) {
            setState(() {
              if (index != 2) {
                ExpenseListModel.of(context).reset();
                _currentPageIndex = index;
              }

              _controller.reverse();
            });
          },
          fixedColor: ColorPallet.lightBlue,
          type: customBottomNavigationBar.BottomNavigationBarType.fixed,
          currentIndex: _currentPageIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.event_note),
                title: Text(
                  translate.overviewPage,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 13.0 * f),
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.receipt),
                title: Text(
                  translate.expensesPage,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 13.0 * f),
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.add),
                title: Text(
                  '',
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 13.0 * f),
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.equalizer),
                title: Text(
                  translate.insightsPage,
                  style: TextStyle(
                      fontWeight: FontWeight.w600, fontSize: 13.0 * f),
                )),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                size: 32.0 * x,
              ),
              title: Text(
                translate.settingsPage,
                style:
                    TextStyle(fontWeight: FontWeight.w600, fontSize: 13.0 * f),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
