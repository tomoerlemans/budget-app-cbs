import 'package:budget_onderzoek/util/translate.dart';
import 'package:flutter/material.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';

TextStyle headerText;
TextStyle settingText;
TextStyle valueText;

double x;
double y;
double f;

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingsPageState();
  }
}

class _SettingsPageState extends State<SettingsPage> {
  Widget getFamilyMember(String name) {
    return Container(
        decoration: BoxDecoration(
          border: Border.all(width: 1.0 * x, color: ColorPallet.midGray),
        ),
        child: Row(
          children: <Widget>[
            Text(name),
            SizedBox(width: 10.0 * x),
            Icon(Icons.close, size: 24.0 * x),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    headerText = TextStyle(
        color: ColorPallet.darkBlue,
        fontSize: 18.0 * f,
        fontWeight: FontWeight.w700);
    settingText = TextStyle(
        color: ColorPallet.darkBlue,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);
    valueText = TextStyle(
        color: ColorPallet.midGray,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);

    return Column(
      children: <Widget>[
        _TopBarWidget(),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(bottom: 8.0),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 8.0 * y),
                _PreferencesWidget(),
                SizedBox(height: 8.0 * y),
                // _FamilyWidget(),
                // SizedBox(height: 8.0 * y),
                _NotificationWidget(),
                SizedBox(height: 8.0 * y),
                _ContactWidget(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.lightBlue,
      height: 50 * y,
      child: Row(
        children: <Widget>[
          SizedBox(width: 22.0 * x),
          Text(
            translate.settings,
            style: TextStyle(
                color: Colors.white,
                fontSize: 22.0 * f,
                fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}

class _PreferencesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _SettingsBoxWidget(
      settingsWidget: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.tune, color: ColorPallet.darkBlue, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translate.preferences, style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.language, style: settingText),
                Text(translate.languageValue, style: valueText)
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.currency, style: settingText),
                Text("Euro", style: valueText)
              ],
            ),
            SizedBox(height: 8.0),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    );
  }
}

class _FamilyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.group,
                      color: ColorPallet.darkBlue, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translate.family, style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.familyCode, style: settingText),
                  Text("3967-5467-8926", style: valueText)
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.shareWithFamily, style: settingText),
                  Text(translate.onValue, style: valueText)
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.familyMembers, style: settingText),
                  ButtonTheme(
                    height: 37.0 * y,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0 * x),
                      ),
                      color: ColorPallet.lightBlue,
                      child: Text(translate.add,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 16.0 * f)),
                      onPressed: () {},
                    ),
                  )
                ],
              ),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0 * x),
                      border: Border.all(
                          width: 2.5 * x, color: ColorPallet.darkBlue),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 16.0 * x, vertical: 3.0 * y),
                      child: Row(
                        children: <Widget>[
                          Text("Kim", style: settingText),
                          SizedBox(width: 10.0 * x),
                          Icon(Icons.close, size: 24.0 * x),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0 * x),
                      border: Border.all(
                          width: 2.5 * x, color: ColorPallet.darkBlue),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 12.0 * x, vertical: 3.0 * y),
                      child: Row(
                        children: <Widget>[
                          Text("Dexter", style: settingText),
                          SizedBox(width: 10.0 * x),
                          Icon(Icons.close, size: 24.0 * x),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0 * x),
                      border: Border.all(
                          width: 2.5 * x, color: ColorPallet.darkBlue),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 12.0 * x, vertical: 3.0 * y),
                      child: Row(
                        children: <Widget>[
                          Text("Karin", style: settingText),
                          SizedBox(width: 10.0 * x),
                          Icon(
                            Icons.close,
                            size: 24.0 * x,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      ),
    );
  }
}

class _NotificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.notifications,
                  color: ColorPallet.darkBlue, size: 24.0 * x),
              SizedBox(width: 10.0 * x),
              Text(translate.notifications, style: headerText),
            ],
          ),
          SizedBox(height: 24.0 * y),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(translate.dailyReminders, style: settingText),
              Text(translate.onValue, style: valueText)
            ],
          ),
          SizedBox(height: 8.0 * y),
          Container(height: 1.0 * y, color: ColorPallet.lightGray),
          SizedBox(height: 24.0 * y),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(translate.time, style: settingText),
              Text("20:30", style: valueText)
            ],
          ),
          SizedBox(height: 8.0 * y),
          Container(height: 1.0 * y, color: ColorPallet.lightGray),
        ],
      ),
    ));
  }
}

class _ContactWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _SettingsBoxWidget(
      settingsWidget: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.forum, color: ColorPallet.darkBlue, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translate.contact, style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * x),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.phoneNumber, style: settingText),
                Text("070 337 3800", style: valueText)
              ],
            ),
            SizedBox(height: 8.0 * x),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.email, style: settingText),
                Text("support@cbs.nl", style: valueText)
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.sendMessage, style: settingText),
                Icon(Icons.expand_more,
                    color: ColorPallet.midGray, size: 24.0 * x)
              ],
            ),
            SizedBox(height: 8.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    );
  }
}

class _SettingsBoxWidget extends StatelessWidget {
  _SettingsBoxWidget({@required this.settingsWidget});

  final Widget settingsWidget;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
      child: settingsWidget,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: ColorPallet.veryLightGray,
              offset: Offset(5.0 * x, 1.0 * y),
              blurRadius: 2.0 * x,
              spreadRadius: 3.0 * x)
        ],
      ),
    );
  }
}
