import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'package:budget_onderzoek/util/color_pallet.dart';

final charts.Color myBlue = charts.Color(
    r: ColorPallet.lightBlue.red,
    g: ColorPallet.lightBlue.green,
    b: ColorPallet.lightBlue.blue,
    a: ColorPallet.lightBlue.alpha);

final charts.Color myGreen = charts.Color(
    r: ColorPallet.lightGreen.red,
    g: ColorPallet.lightGreen.green,
    b: ColorPallet.lightGreen.blue,
    a: ColorPallet.lightGreen.alpha);

final charts.Color myOrange = charts.Color(
    r: ColorPallet.orange.red,
    g: ColorPallet.orange.green,
    b: ColorPallet.orange.blue,
    a: ColorPallet.orange.alpha);

final charts.Color myPink = charts.Color(
    r: ColorPallet.pink.red,
    g: ColorPallet.pink.green,
    b: ColorPallet.pink.blue,
    a: ColorPallet.pink.alpha);

final charts.Color myDarkBlue = charts.Color(
    r: ColorPallet.darkBlue.red,
    g: ColorPallet.darkBlue.green,
    b: ColorPallet.darkBlue.blue,
    a: ColorPallet.darkBlue.alpha);

double x;
double y;
double f;

class BarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  BarChart(this.seriesList, {this.animate});

  /// Creates a stacked [BarChart] with sample data and no transition.
  factory BarChart.withSampleData() {
    return BarChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    x = MediaQuery.of(context).size.width / 411.42857142857144;
    y = MediaQuery.of(context).size.height / 683.4285714285714;
    f = (x + y) / 2;

    return charts.BarChart(
      seriesList,
      animate: animate,
      barGroupingType: charts.BarGroupingType.stacked,
      primaryMeasureAxis: charts.NumericAxisSpec(
        renderSpec: charts.GridlineRendererSpec(

            labelStyle: charts.TextStyleSpec(
                color: myDarkBlue,
                fontFamily: 'Source Sans Pro Semibold',
                fontSize: (14.0 * f).floor())),
        showAxisLine: false,
        tickProviderSpec: charts.BasicNumericTickProviderSpec(
          desiredMinTickCount: 3,
        ),
      ),
      domainAxis: charts.OrdinalAxisSpec(
        viewport: charts.OrdinalViewport('1', 13),
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: charts.TextStyleSpec(
              fontFamily: 'Source Sans Pro Semibold',
              color: myDarkBlue,
              fontSize: (14.0 * f).floor()),
        ),
      ),
      behaviors: [
        charts.SlidingViewport(),
        charts.PanAndZoomBehavior(),
      ],
    );
  }

  /// Create series list with multiple series
  static List<charts.Series<ExpenseDate, String>> _createSampleData() {
    final leukeUitgaven = [
      new ExpenseDate('1', 5),
      new ExpenseDate('2', 25),
      new ExpenseDate('3', 0),
      new ExpenseDate('4', 75),
      new ExpenseDate('5', 5),
      new ExpenseDate('6', 0),
      new ExpenseDate('7', 0),
      new ExpenseDate('8', 0),
      new ExpenseDate('9', 5),
      new ExpenseDate('10', 25),
      new ExpenseDate('11', 200),
      new ExpenseDate('12', 0),
      new ExpenseDate('13', 0),
      new ExpenseDate('14', 0),
      new ExpenseDate('15', 0),
      new ExpenseDate('16', 0),
      new ExpenseDate('17', 0),
      new ExpenseDate('18', 0),
      new ExpenseDate('19', 0),
      new ExpenseDate('20', 0),
      new ExpenseDate('21', 0),
      new ExpenseDate('22', 0),
      new ExpenseDate('23', 0),
      new ExpenseDate('24', 30),
      new ExpenseDate('25', 0),
      new ExpenseDate('26', 0),
      new ExpenseDate('27', 0),
      new ExpenseDate('28', 0),
      new ExpenseDate('29', 0),
      new ExpenseDate('30', 0),
      new ExpenseDate('31', 0),
    ];

    final ontwikkelingUitgaven = [
      new ExpenseDate('1', 0),
      new ExpenseDate('2', 0),
      new ExpenseDate('3', 0),
      new ExpenseDate('4', 100),
      new ExpenseDate('5', 0),
      new ExpenseDate('6', 0),
      new ExpenseDate('7', 0),
      new ExpenseDate('8', 0),
      new ExpenseDate('9', 0),
      new ExpenseDate('10', 200),
      new ExpenseDate('11', 0),
      new ExpenseDate('12', 0),
      new ExpenseDate('13', 0),
      new ExpenseDate('14', 0),
      new ExpenseDate('15', 0),
      new ExpenseDate('16', 0),
      new ExpenseDate('17', 0),
      new ExpenseDate('18', 0),
      new ExpenseDate('19', 0),
      new ExpenseDate('20', 0),
      new ExpenseDate('21', 0),
      new ExpenseDate('22', 0),
      new ExpenseDate('23', 0),
      new ExpenseDate('24', 0),
      new ExpenseDate('25', 0),
      new ExpenseDate('26', 0),
      new ExpenseDate('27', 0),
      new ExpenseDate('28', 0),
      new ExpenseDate('29', 0),
      new ExpenseDate('30', 100),
      new ExpenseDate('31', 0),
    ];

    final dagelijkseBoodschappen = [
      new ExpenseDate('1', 30),
      new ExpenseDate('2', 20),
      new ExpenseDate('3', 20),
      new ExpenseDate('4', 0),
      new ExpenseDate('5', 0),
      new ExpenseDate('6', 50),
      new ExpenseDate('7', 10),
      new ExpenseDate('8', 0),
      new ExpenseDate('9', 0),
      new ExpenseDate('10', 5),
      new ExpenseDate('11', 0),
      new ExpenseDate('12', 8),
      new ExpenseDate('13', 0),
      new ExpenseDate('14', 0),
      new ExpenseDate('15', 0),
      new ExpenseDate('16', 0),
      new ExpenseDate('17', 0),
      new ExpenseDate('18', 0),
      new ExpenseDate('19', 0),
      new ExpenseDate('20', 0),
      new ExpenseDate('21', 0),
      new ExpenseDate('22', 0),
      new ExpenseDate('23', 0),
      new ExpenseDate('24', 0),
      new ExpenseDate('25', 0),
      new ExpenseDate('26', 0),
      new ExpenseDate('27', 0),
      new ExpenseDate('28', 0),
      new ExpenseDate('29', 0),
      new ExpenseDate('30', 10),
      new ExpenseDate('31', 0),
    ];

    final vasteLasten = [
      new ExpenseDate('1', 200),
      new ExpenseDate('2', 0),
      new ExpenseDate('3', 0),
      new ExpenseDate('4', 0),
      new ExpenseDate('5', 150),
      new ExpenseDate('6', 0),
      new ExpenseDate('7', 0),
      new ExpenseDate('8', 0),
      new ExpenseDate('9', 600),
      new ExpenseDate('10', 0),
      new ExpenseDate('11', 0),
      new ExpenseDate('12', 0),
      new ExpenseDate('13', 0),
      new ExpenseDate('14', 0),
      new ExpenseDate('15', 0),
      new ExpenseDate('16', 60),
      new ExpenseDate('17', 0),
      new ExpenseDate('18', 0),
      new ExpenseDate('19', 20),
      new ExpenseDate('20', 0),
      new ExpenseDate('21', 0),
      new ExpenseDate('22', 0),
      new ExpenseDate('23', 0),
      new ExpenseDate('24', 20),
      new ExpenseDate('25', 0),
      new ExpenseDate('26', 0),
      new ExpenseDate('27', 0),
      new ExpenseDate('28', 0),
      new ExpenseDate('29', 0),
      new ExpenseDate('30', 20),
      new ExpenseDate('31', 0),
    ];

    return [
      new charts.Series<ExpenseDate, String>(
        id: 'leukeUitgaven',
        domainFn: (ExpenseDate sales, _) => sales.date,
        measureFn: (ExpenseDate sales, _) => sales.expense,
        data: leukeUitgaven,
        colorFn: (ExpenseDate sales, _) => myOrange,
      ),
      new charts.Series<ExpenseDate, String>(
        id: 'ontwikkelingUitgaven',
        domainFn: (ExpenseDate sales, _) => sales.date,
        measureFn: (ExpenseDate sales, _) => sales.expense,
        data: ontwikkelingUitgaven,
        colorFn: (ExpenseDate sales, _) => myBlue,
      ),
      new charts.Series<ExpenseDate, String>(
          id: 'dagelijkseBoodschappen',
          domainFn: (ExpenseDate sales, _) => sales.date,
          measureFn: (ExpenseDate sales, _) => sales.expense,
          data: dagelijkseBoodschappen,
          colorFn: (ExpenseDate sales, _) => myGreen),
      new charts.Series<ExpenseDate, String>(
          id: 'vasteLasten',
          domainFn: (ExpenseDate sales, _) => sales.date,
          measureFn: (ExpenseDate sales, _) => sales.expense,
          data: vasteLasten,
          colorFn: (ExpenseDate sales, _) => myPink),
    ];
  }
}

class ExpenseDate {
  final String date;
  final int expense;

  ExpenseDate(this.date, this.expense);
}
